// 导入组件
import Vue from 'vue';
import Router from 'vue-router';

// 首页
import index from '@/views/index';
import main from '@/views/main';

/**
 * 基础菜单
 */
// 产品管理
import Categories from '@/views/product/Categories.vue';
import Products from '@/views/product/Products';
import Devices from '@/views/device/Devices';
import DeviceDetail from '@/views/device/DeviceDetail';
import VirtualDevices from '@/views/device/VirtualDevices';
import VirtualDeviceDetail from '@/views/device/VirtualDeviceDetail';
import DeviceGroups from '@/views/device/DeviceGroups';
import AppDesign from '@/views/app/AppDesign';
import DesignDetail from '@/views/app/DesignDetail';
import Convertors from '@/views/protocol/Convertors';
import Components from '@/views/protocol/Components';

// 空间管理
import Spaces from '@/views/space/Spaces.vue';
// 用户管理
import Users from '@/views/system/Users.vue';
// C端用户管理
import Cusers from '@/views/client/Users.vue';

//天猫精灵产品
import AligenieProducts from '@/views/aligenie/Products';

//数据大屏
import dataScreen from '@/views/dataView/Screen'
//数据统计
import dataStatistics from '@/views/dataView/Statistics'
//规则列表
import Rules from '@/views/rule/Rules'
//告警列表
import Alarms from '@/views/alarm/Alarms'
//告警设置
import AlarmSettings from '@/views/alarm/AlarmSettings'


//定时任务
import Tasks from '@/views/rule/Tasks'

// 启用路由
Vue.use(Router);

// 导出路由
const router =new Router({
  routes: [
  {
    path: '/',
    name: '',
    component: index,
    hidden: true,
    meta: {
      requireAuth: false
    },
    redirect:'main'
  }, 
  {
    path: '/index',
    name: '首页',
    component: index,
    iconCls: 'el-icon-tickets',
    children: [
    {
      path:'/main',
      name:'main',
      component:main
    },
    {
        path: '/product/Categories',
        name: '品类管理',
        component: Categories,
        meta: {
          requireAuth: true
        }
      }, {
        path: '/product/Products',
        name: '产品管理',
        component: Products,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/protocol/Convertors',
        name: '转换器管理',
        component: Convertors,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/protocol/Components',
        name: '通讯组件管理',
        component: Components,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/device/Devices',
        name: '设备管理',
        component: Devices,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/device/Groups',
        name: '设备分组',
        component: DeviceGroups,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/device/DeviceDetail',
        name: '设备详情',
        component: DeviceDetail,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/device/VirtualDevices',
        name: '虚拟设备',
        component: VirtualDevices,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/device/VirtualDeviceDetail',
        name: '虚拟设备详情',
        component: VirtualDeviceDetail,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/space/Spaces',
        name: '空间管理',
        component: Spaces,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/system/Users',
        name: '用户管理',
        component: Users,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/client/Users',
        name: 'C端用户管理',
        component: Cusers,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/app/AppDesign',
        name: 'app定制',
        component: AppDesign,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/app/DesignDetail',
        name: '定制详情',
        component: DesignDetail,
        meta: {
          requireAuth: true
        }
      },
      {
        path: '/aligenie/Products',
        name: '天猫精灵接入产品列表',
        component: AligenieProducts,
        meta: {
          requireAuth: true
        }
      }, 
      {
        path: '/dataView/Screen',
        name: '数据大屏',
        component: dataScreen,
      },
      {
        path: '/dataView/Statistics',
        name: '数据统计',
        component: dataStatistics,
      },
      {
        path: '/rule/Rules',
        name: '规则列表',
        component: Rules,
      },
      {
        path: '/alarm/Alarms',
        name: '告警列表',
        component: Alarms,
      },
      {
        path: '/alarm/AlarmSettings',
        name: '告警设置',
        component: AlarmSettings,
      },
      {
        path: '/rule/Tasks',
        name: '定时任务',
        component: Tasks,
        meta: {
          requireAuth: true
        }
      }
    ]
  }]
})

router.beforeEach(async (to, from, next) => {
  next()
})

export default router
